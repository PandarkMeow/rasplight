<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Centre de contrôle Raspberry</title>
    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <!--<script src="main.js"></script>-->
</head>
<?php
require "etat.php";

try {
    $bdd = new PDO('mysql:host=localhost;dbname=rapsberry;charset=utf8', 'root', 'root');
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$reponse = $bdd->prepare('SELECT * FROM rapsberry');
$reponse->execute();
$result = array();
while ($donnees = $reponse->fetch()) {
    array_push($result, array('id' => $donnees['id'], 'port' => $donnees['port'], 'val' => $donnees['val']));
}
$reponse->closeCursor();
?>
<body>
<div class="conteneur">
    <h1>Centre de contrôle</h1>
    <img src="img/logo.png" alt="">
    <div class="controles">
        <div class="red">
            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
            <button type="button" name="button"></button>
        </div>
        <div class="green">
            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
            <button type="button" name="button"></button>
        </div>
    </div>
</div>
<div class="test">
    <?php
    $etat = new Etat();
    $led = $etat->getPort($result);
    
    foreach ($result as $key => $value) {
		$port = $value['port'];
		$val = $value['val'];
     }
    
   
    var_dump($port) ;

    $_POST['port']=$port;
    $_POST['val']=$val;

	$_POST['action']='allumer';
	
	var_dump($_POST) ;
	
/*
 * mettre valeur sur le bouton passer en post
 * condition sur la valeur
 * lancer methode
 * */	
	
	
	
//   var_dump($etat->getPort($result));

    /*
        $result= $etat->allumer($result,7);
        $etat->getPort($result);


        $result= $etat->look($result);
        $etat->getPort($result);


        $result= $etat->eteindre($result,7);
        $etat->getPort($result);
    */

    /*
    $result= $etat->allReinitiate($result);
    var_dump($result);
    $etat->getPort($result);


    $result= $etat->allLight($result);
    var_dump($result);
    $etat->getPort($result);
    */

    ?>
</div>
</body>
</html>
