<?php
/**
 * Created by PhpStorm.
 * User: Doreen
 */

/**
 *class qui permet de gerer les etats d'une led
 */
class Etat
{
	public static $reponse='';
	
     /**
     * établi la reponse
     */
    function start()
    {
          
		if (isset($_POST['action']) && !empty($_POST['action'])) {
			$reponse = array($_POST['port'],$_POST['val'] );
			$reponse = $this->allumer($_POST['port']);
        }
             
        var_dump($reponse);
        return $reponse;
    }


///////////////////////////////////// CONNEXION
    /**
     * établi la connexion
     * @return PDO
     */
    function connexion()
    {
        try {
            $bdd = new PDO('mysql:host=localhost;dbname=rapsberry;charset=utf8', 'root', 'root');
            
            
		if (isset($_POST['led']) && !empty($_POST['led'])) {
			$led = $_POST['led'];
        }
            
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $bdd;
    }


/////////////////////////////////////// GESTION VUE SUR LES LEDS

    /**
     * retourne tous les ports de la bdd
     * @param $reponse
     * @return array
     */
    function getPort($reponse)
    {
        $ledlist = array();
        foreach ($reponse as $key => $value) {
            array_push($ledlist, $value);
        }
        return $ledlist;
    }
    

    /**
     * regarde si la led est allumé ou non, et l'allume ou non
     * @param $reponse
     */
    function look($reponse)
    {
        foreach ($reponse as $led => $value) {
            if ($value['val'] == 1) {
                exec('gpio write ' . $value['val'] . '1');
                var_dump('allumée');
            } else {
                exec('gpio write' . $value['val'] . '0');
                var_dump('éteint');
            }
        }
    }


/////////////////////////////////////////// GESTION LED

    /**
     * allume la led pour un port donné
     * @param $reponse
     * @param $port
     * @return array
     */
    function allumer($port)
    {
        foreach ($reponse as $led => $value) {
            if ($value['port'] == $port) {
                exec('gpio write ' . $value['port'] . '1');
                var_dump($port . ' allumée');

                $key = array_search($value['port'], $reponse);
                $reponse[$key]['val'] = 1;
            }
        }
        return ($this->update($reponse));
    }

    /**
     * éteint la led pour un port donné
     * @param $reponse
     * @param $port
     * @return array
     */
    function eteindre($port)
    {
        foreach ($reponse as $led => $value) {
            if ($value['port'] == $port) {
                exec('gpio write ' . $value['val'] . '0');
                var_dump($port . ' éteint');


                $key = array_search($value['port'], $reponse);
                $reponse[$key]['val'] = 0;
            }
        }
        var_dump($this->update($reponse));
        return ($this->update($reponse));
    }


    /**
     * éteint toutes les leds
     * @param $reponse
     */
    function allReinitiate()
    {
        foreach ($reponse as $led => $value) {
            exec('gpio write ' . $value['val'] . '0');
            $key = array_search($value['port'], $reponse);
            $reponse[$key]['val'] = 0;
        }
        $this->update($reponse);
    }

    /**
     * allume toutes les leds
     * @param $reponse
     */
    function allLight()
    {
        foreach ($reponse as $led => $value) {
            exec('gpio write ' . $value['val'] . '1');
            $key = array_search($value['port'], $reponse);
            $reponse[$key]['val'] = 0;
        }
        $this->update($reponse);
    }


/////////////////////////////////////// GESTION BDD

    /**
     * mise a jour de bdd
     * @param $reponse
     * @return array
     */
    function update()
    {
        $bdd = $this->connexion();
        foreach ($reponse as $led => $value) {
            if ($value['val'] == 1) {
                $sql = 'UPDATE rapsberry.rapsberry SET val=1  WHERE port=' . $value['port'] . ';';
                var_dump('update ' . $value['port'] . ' allumée');

            } else {
                $sql = 'UPDATE rapsberry.rapsberry SET val=0 WHERE port=' . $value['port'] . ';';
                var_dump('update ' . $value['port'] . ' éteint');

            }
            $reponse = $bdd->prepare($sql);
            $reponse->execute();
        }
        /**
         * mise a jour de la liste des leds
         */
        $reponse = $bdd->prepare('SELECT * FROM rapsberry');
        $reponse->execute();
        $result = array();
        while ($donnees = $reponse->fetch()) {
            array_push($result, array('id' => $donnees['id'], 'port' => $donnees['port'], 'val' => $donnees['val']));
        }

        $reponse->closeCursor();
        return $result;
    }


}
