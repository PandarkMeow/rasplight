setup ()
{
 echo Setup
 gpio -g mode 4 in
 gpio -g mode 26 out
}

waitButton ()
{
 echo -n "Waiting for button ... "
 if [ `gpio -g read 4` = 1 ]
 then
   gpio -g write 26 1
 else
  gpio -g write 26 0
 fi
}

setup
while true; do
 waitButton
done
